[![License](https://img.shields.io/badge/License-BSD%202--Clause-blue.svg)](https://opensource.org/licenses/BSD-2-Clause)

EfficientIP Ansible Collection
==============================

This is a collection of plugins and modules to interact with [EfficientIP](https://www.efficientip.com/products/) products. 

Requirements
------------

SOLIDserverRest v2.0 and above

Example Playbook
----------------

```
- name: Example usage of solidserver_ipam_ip module
  hosts: servers
  vars:
    solidserver:
      host: solidserver.example.net
      authentication: basic
      username: your_username
      password: your_password
      check_tls: true
      timeout: 5
  connection: local
  tasks:
  - name: Allocate an IP address in the IPAM subnet 192.168.1.0/24
    community.efficientip.solidserver_ipam_ip:
      space: Local
      subnet: 192.168.1.0/24
      pool_name: pool_1
      hostname: host_1
      ip: dynamic
      mac: 01:02:03:04:05:06
      aliases:
        alias_a1: a
        alias_a2: a
        alias_cname1: cname
        alias_cname2: cname
      class_name: ansible
      class_parameters:
        param1: value1
        param2: value2
      update: true
      state: present
      provider: "{{ solidserver }}"
```

License
-------

BSD-2-Clause
