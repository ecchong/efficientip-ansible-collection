# -*- coding: utf-8 -*-
#
# This module_utils is PRIVATE and should only be used with EfficientIP module.
# Breaking changes can occur any time.

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
__all__ = ['solidserver', 'ip_version', 'result_ip', 'IPVMAP']

DOCUMENTATION = r'''
---
module: solidserver_utils
short_description: SOLIDserver utils module.
requirements:
  - Python Lib for SOLIDserver: SOLIDserverRest v2.0 and above
description:
  - This module is PRIVATE and should only be used with other EfficientIP modules.
extends_documentation_fragment: nothing
'''

import logging
import socket
import urllib
from http import HTTPStatus
from SOLIDserverRest import SOLIDserverRest


class solidserver:
    sds = None
    timeout = 5

    def __init__(self, provider):
        self.sds = SOLIDserverRest(provider['host'])
        if 'authentication' not in provider or provider['authentication'] != 'native':
            self.sds.use_basicauth_sds(provider['username'], provider['password'])
        else:
            self.sds.use_native_sds(provider['username'], provider['password'])
        self.sds.set_ssl_verify('check_tls' not in provider or provider['check_tls'] is not False)
        if 'proxy' in provider:
            self.sds.set_proxy(provider['proxy'])
        if 'certificate' in provider:
            self.sds.set_certificate_file(provider['certificate'])
        if 'timeout' in provider:
            self.timeout = int(provider['timeout'])

    def query(self, query, params=''):
        if self.sds is None:
            return (0, 'SOLIDserver is not defined')
        res = self.sds.query(query, params, self.timeout)
        try:
            json = res.json()
        except ValueError:   # pragma: no cover
            err = 'Status ' + str(res.status_code) + ': ' + HTTPStatus(res.status_code).phrase
            if res.status_code >= 200 and res.status_code < 300:
                return (res.status_code, err + ' (no data from SOLIDserver)')
            return (res.status_code, err)
        return (res.status_code, json)

    def ip_site_list(self, order=None, limit=None, offset=None):
        p = {}
        if order is not None:
            p['ORDERBY'] = order
        if limit is not None:
            p['limit'] = limit
        if offset is not None:
            p['offset'] = offset
        code, json = self.query('ip_site_list', p)
        return (type(json) == list, code, json)

    def ip_subnet_list(self, ipv, space, terminal=True, order=None, limit=None, offset=None):
        w = []
        w.append("site_name='" + space + "'")
        if terminal:
            w.append("is_terminal='1'")
        p = {'WHERE': ' and '.join(w)}
        if order is not None:
            p['ORDERBY'] = order
        if limit is not None:
            p['limit'] = limit
        if offset is not None:
            p['offset'] = offset
        code, json = self.query(IPVMAP(ipv, 'ip_subnet_list'), p)
        return (type(json) == list, code, json)

    def ip_subnet_get(self, ipv, space, subnet_start, subnet_prefix, terminal=True):
        p = []
        p.append("site_name='" + space + "'")
        if (ipv == 6):
            p.append("start_hostaddr='" + subnet_start + "'")
            p.append("subnet6_prefix=" + str(subnet_prefix))
        else:
            p.append("start_ip_addr='" + str(ip2hex(subnet_start)) + "'")
            p.append("subnet_size=" + str(prefix2size(subnet_prefix)))
        if terminal is True:
            p.append("is_terminal='1'")
        code, json = self.query(IPVMAP(ipv, 'ip_subnet_list'), {'WHERE': ' and '.join(p)})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_pool_get(self, ipv, subnet, pool_name):
        p = []
        p.append(IPVMAP(ipv, 'subnet_id') + '=' + subnet)
        p.append(IPVMAP(ipv, 'pool_name') + "='" + pool_name + "'")
        code, json = self.query(IPVMAP(ipv, 'ip_pool_list'), {'WHERE': ' and '.join(p)})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_from_name(self, ipv, space, subnet, name, pool=None):
        p = []
        p.append("site_name='" + space + "'")
        p.append(IPVMAP(ipv, 'subnet_id') + '=' + subnet)
        p.append(IPVMAP(ipv, 'name') + "='" + name + "'")
        p.append("type<>'free'")
        if pool is not None:
            p.append(IPVMAP(ipv, 'pool_id') + '=' + pool)
        code, json = self.query(IPVMAP(ipv, 'ip_address_list'), {'WHERE': ' and '.join(p)})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_from_addr(self, ipv, space, addr):
        p = []
        p.append("site_name='" + space + "'")
        p.append("hostaddr='" + addr + "'")
        p.append("type<>'free'")
        code, json = self.query(IPVMAP(ipv, 'ip_address_list'), {'WHERE': ' and '.join(p)})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_from_id(self, ipv, id):
        code, json = self.query(IPVMAP(ipv, 'ip_address_info'), {IPVMAP(ipv, 'ip_id'): id})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_del(self, ipv, ip_id):
        code, json = self.query(IPVMAP(ipv, 'ip_address_delete'), {IPVMAP(ipv, 'ip_id'): ip_id})
        if self.json_oid(json):
            return (True, code, json[0])
        return (False, code, json)

    def ip_find(self, ipv, subnet, pool=None, max=15):
        p = {IPVMAP(ipv, 'subnet_id'): subnet, 'max_find': max}
        if pool is not None:
            p[IPVMAP(ipv, 'pool_id')] = pool
        code, json = self.query(IPVMAP(ipv, 'ip_address_find_free'), p)
        return (True, code, json)

    def ip_add(self, ipv, space, addr, name=None, mac=None, class_name=None, class_parameters=None, create=True):
        p = {'site_name': space, 'hostaddr': addr}
        if name is not None:
            p[IPVMAP(ipv, 'name')] = name
        if mac is not None:
            p[IPVMAP(ipv, 'mac_addr')] = mac
        if class_name is not None:
            p[IPVMAP(ipv, 'ip_class_name')] = class_name
        if class_parameters is not None:
            p[IPVMAP(ipv, 'ip_class_parameters')] = urllib.parse.urlencode(class_parameters)
        if create is False:
            svc = 'ip_address_update'
        else:
            svc = 'ip_address_create'
        code, json = self.query(IPVMAP(ipv, svc), p)
        if self.json_oid(json):
            return (True, code, json[0])
        return (False, code, json)

    def ip_dynamic(self, ipv, space, subnet, pool=None, name=None, mac=None, class_name=None, class_parameters=None, max_find=15):
        res, code, json = self.ip_find(ipv, subnet, pool, max_find)
        if res is True:
            for addr in json:
                if IPVMAP(ipv, 'hostaddr') in addr:
                    ip_res, ip_code, ip = self.ip_add(ipv, space, addr[IPVMAP(ipv, 'hostaddr')], name, mac, class_name, class_parameters)
                    if (ip_res is True):
                        return (ip_res, ip_code, ip)
        res = False
        return (res, code, json)

    def ip_alias_add(self, ipv, ip_id, alias, alias_type='CNAME'):
        p = {IPVMAP(ipv, 'ip_id'): ip_id, IPVMAP(ipv, 'ip_name'): alias, IPVMAP(ipv, 'ip_name_type'): alias_type.upper()}
        code, json = self.query(IPVMAP(ipv, 'ip_alias_create'), p)
        if self.json_oid(json):
            return (True, code, json[0])
        return (False, code, json)

    def ip_alias_clear(self, ipv, ip_id):
        code, json = self.query(IPVMAP(ipv, 'ip_alias_list'), {IPVMAP(ipv, 'ip_id'): ip_id})
        if type(json) == list:
            for alias in json:
                self.query(IPVMAP(ipv, 'ip_alias_delete'), {IPVMAP(ipv, 'ip_name_id'): alias[IPVMAP(ipv, 'ip_name_id')]})
            return (True, code, json)
        return (False, code, json)

    def device_add(self, name, space_id, ip, mac=None, nic_number=0):
        device_id = None
        p = []
        p.append("hostdev_name='" + name + "'")
        p.append('hostdev_site_id=' + space_id)
        code, json = self.query('host_device_list', {'WHERE': ' and '.join(p)})
        if type(json) == list and int(json[0]['hostdev_id']) > 0:
            device_id = int(json[0]['hostdev_id'])
        else:
            code, json = self.query('host_device_create', {'hostdev_name': name, 'hostdev_site_id': space_id})
            if self.json_oid(json):
                device_id = json[0]['ret_oid']
        if device_id is not None:
            p = {'hostdev_id': device_id}
            p['site_id'] = space_id
            p['hostiface_addr'] = ip
            p['hostiface_type'] = 'interface'
            p['hostiface_name'] = 'nic' + str(nic_number)
            if mac is not None:
                p['hostiface_mac'] = mac
            code, json = self.query('host_iface_create', p)
            return (True, code, json[0])
        return (False, code, json)

    def json_oid(self, json):
        return (type(json) == list and 'ret_oid' in json[0] and int(json[0]['ret_oid']))


def ip_version(ip):
    try:
        socket.inet_pton(socket.AF_INET, ip)
        return 4
    except socket.error:
        try:
            socket.inet_pton(socket.AF_INET6, ip)
            return 6
        except socket.error:
            return None


def ip2hex(ip):
    ip = ip.split('.')
    if len(ip) != 4:
        return '00000000'
    return ''.join('{:02x}'.format(int(i)) for i in ip)


def prefix2size(prefix):
    pfx = int(prefix)
    if pfx < 0 or pfx > 32:
        return 0
    return 1 << (32 - pfx)


def size2prefix(size):
    pfx = 32
    while (pfx and not (int(size) & (1 << (32 - pfx)))):
        pfx -= 1
    return pfx


def result_ip(result, ip):
    if 'mac_addr' in ip and ip['mac_addr'][0:3] == 'EIP':
        ip.pop('mac_addr')
    if 'address' in ip:
        result['address'] = ip['hostaddr']
    result['ip'] = ip
    return result


IPV6MAP = {
    'hostaddr': 'hostaddr6',
    'ip_address_create': 'ip_address6_create',
    'ip_address_delete': 'ip_address6_delete',
    'ip_address_find_free': 'ip_address6_find_free',
    'ip_address_info': 'ip_address6_info',
    'ip_address_list': 'ip_address6_list',
    'ip_address_update': 'ip_address6_update',
    'ip_alias_create': 'ip_alias6_create',
    'ip_alias_delete': 'ip_alias6_delete',
    'ip_alias_list': 'ip_alias6_list',
    'ip_class_name': 'ip6_class_name',
    'ip_class_parameters': 'ip6_class_parameters',
    'ip_id': 'ip6_id',
    'ip_name': 'ip6_name',
    'ip_name_type': 'ip6_name_type',
    'ip_name_id': 'ip6_name_id',
    'ip_pool_list': 'ip_pool6_list',
    'ip_subnet_list': 'ip_subnet6_list',
    'mac_addr': 'ip6_mac_addr',
    'name': 'ip6_name',
    'pool_id': 'pool6_id',
    'pool_name': 'pool6_name',
    'subnet_id': 'subnet6_id',
}


def IPVMAP(ipv, svc):
    if ipv == 6:
        return IPV6MAP[svc]
    return svc


def dbg(msg):
    logging.warning(msg)
