from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
lookup: solidserver_ipam_ip_network_list
short_description: Look up the IP networks in SOLIDserver IPAM.
description:
  - This module produces a list of all the available IPAM networks.
options:
  host:
    description: 'SOLIDserver hostname or IP address'
  authentication:
    description: 'Authentication mode (basic|native)'
  username:
    description: 'Authentication username'
  password:
    description: 'Authentication password'
  check_tls:
    description: 'Check SSL certificate (default: True)'
  space:
    description: 'IPAM space'
  terminal:
    description: 'Only terminal networks (default: True)'
  ip_version:
    description: 'IP version (default: 4)'
  order:
    description: 'Sort order'
  limit:
    description: 'Results limit'
  offset:
    description: 'Results offset'
'''

EXAMPLES = """
vars:
  spaces: "{{ lookup('solidserver_ipam_ip_network_list', host='192.168.1.1', authentication='basic',
                      username='ipmadmin', password='password', space='Local', wantlist=True) }}"
"""

RETURN = """
_raw:
  description: comma-separated list of IPAM networks
"""


import logging
from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
from ansible_collections.community.efficientip.plugins.module_utils.solidserver import solidserver, size2prefix


class LookupModule(LookupBase):
    def run(self, terms, variables, **kwargs):
        if 'host' not in kwargs:
            raise AnsibleError("Missing SOLIDserver host")
        if 'username' not in kwargs:
            raise AnsibleError("Missing SOLIDserver username")
        if 'password' not in kwargs:
            raise AnsibleError("Missing SOLIDserver password")
        if 'space' not in kwargs:
            raise AnsibleError("Missing SOLIDserver IPAM space")
        if 'terminal' not in kwargs:
            kwargs['terminal'] = True
        order = None
        if 'order' in kwargs:
            order = kwargs['order']
        limit = None
        if 'limit' in kwargs:
            limit = kwargs['limit']
        offset = None
        if 'offset' in kwargs:
            offset = kwargs['offset']
        ipv = 4
        if 'ip_version' in kwargs:
            if kwargs['ip_version'] == 4 or kwargs['ip_version'] == 6:
                ipv = kwargs['ip_version']
            else:
                raise AnsibleError("Invalid IP version: " + kwargs['ip_version'])
        provider = {'host': kwargs['host'], 'username': kwargs['username'], 'password': kwargs['password']}
        if 'authentication' in kwargs and kwargs['authentication'] == 'native':
            provider['authentication'] = 'native'
        provider['check_tls'] = 'check_tls' not in kwargs or kwargs['check_tls'] is not False
        sds = solidserver(provider)
        res, code, json = sds.ip_subnet_list(ipv, kwargs['space'], kwargs['terminal'], order, limit, offset)
        if res is False:
            raise AnsibleError("Failed to retrieve IPAM networks")
        return [network['start_hostaddr'] + '/' + str(size2prefix(network['subnet_size'])) for network in json]
